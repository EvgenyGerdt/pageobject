package TestSuite;

import Pages.MainPage;
import Pages.RegistrationPage;
import Utils.DataGenerator;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RegistrationPageTest extends FunctionalTest {

    /*
    * Проверка регистрации при правильных данных
    * */

    @Test(priority = 4)
    public void successRegistration() {
        DataGenerator data = new DataGenerator();

        driver.get("http://users.bugred.ru/user/login/index.html");

        RegistrationPage regPage = new RegistrationPage(driver);

        regPage.enterUsername(data.generateUsername());
        regPage.enterEmail(data.generateEmail());
        regPage.enterPassword(data.getPassword());

        MainPage mainPage = regPage.submit();

        Assert.assertTrue(mainPage.userBar());
    }

    /*
     * Проверка регистрации без Имени пользователя
     * */

    @Test(priority = 1)
    public void emptyUsernameRegistration() {
        DataGenerator data = new DataGenerator();

        driver.get("http://users.bugred.ru/user/login/index.html");

        RegistrationPage regPage = new RegistrationPage(driver);

        regPage.enterEmail(data.generateEmail());
        regPage.enterPassword(data.getPassword());

        regPage.submit();

        Assert.assertEquals(driver.getCurrentUrl(), "http://users.bugred.ru/user/login/index.html");
    }

    /*
     * Проверка регистрации без почтового адреса
     * */

    @Test(priority = 2)
    public void emptyEmailRegistration() {
        DataGenerator data = new DataGenerator();

        driver.get("http://users.bugred.ru/user/login/index.html");

        RegistrationPage regPage = new RegistrationPage(driver);

        regPage.enterUsername(data.generateUsername());
        regPage.enterPassword(data.getPassword());

        regPage.submit();

        Assert.assertEquals(driver.getCurrentUrl(), "http://users.bugred.ru/user/login/index.html");
    }

    /*
     * Проверка регистрации без пароля
     * */

    @Test(priority = 3)
    public void emptyPasswordRegistration() {
        DataGenerator data = new DataGenerator();

        driver.get("http://users.bugred.ru/user/login/index.html");

        RegistrationPage regPage = new RegistrationPage(driver);

        regPage.enterEmail(data.generateEmail());
        regPage.enterUsername(data.generateUsername());

        regPage.submit();

        Assert.assertEquals(driver.getCurrentUrl(), "http://users.bugred.ru/user/login/index.html");
    }

}
