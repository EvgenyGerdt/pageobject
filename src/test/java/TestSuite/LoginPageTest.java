package TestSuite;

import Pages.LoginPage;
import Pages.MainPage;
import Utils.DataRepository;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginPageTest extends FunctionalTest {

    DataRepository data;

    @Test(priority = 3)
    public void successLogin() {

        driver.get(data.loginPage);

        LoginPage loginPage = new LoginPage(driver);

        loginPage.enterEmail("adminTest@test.org");
        loginPage.enterPassword("12345");

        MainPage mainPage = loginPage.submit();

        Assert.assertTrue(mainPage.userBar());
    }

    @Test(priority = 1)
    public void emptyEmailLogin() {
        driver.get(data.loginPage);

        LoginPage loginPage = new LoginPage(driver);

        loginPage.enterPassword(data.authPassword);

        loginPage.submit();

        Assert.assertEquals(driver.getCurrentUrl(), data.loginPage);
    }

    @Test(priority = 2)
    public void emptyPasswordLogin() {
        driver.get(data.loginPage);

        LoginPage loginPage = new LoginPage(driver);

        loginPage.enterEmail(data.authEmail);

        loginPage.submit();

        Assert.assertEquals(driver.getCurrentUrl(), data.loginPage);
    }

}
