package TestSuite;

import Pages.ProfilePage;
import Utils.DataGenerator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ProfilePageTest extends FunctionalTest {

    private String buff;

    @BeforeClass
    public void signIn() {
        LoginPageTest login = new LoginPageTest();

        login.successLogin();
        driver.get("http://users.bugred.ru/user/profile/index.html");
    }

    @Test(priority = 1)
    public void usernameChange() {

        DataGenerator data = new DataGenerator();

        ProfilePage profilePage = new ProfilePage(driver);

        buff = data.generateUsername();
        profilePage.enterNewUsername(buff);
        profilePage.submit();

        Assert.assertEquals(profilePage.username.getAttribute("value"), buff);
    }

    @Test(priority = 2)
    public void genderChange() {

        ProfilePage profilePage = new ProfilePage(driver);

        profilePage.enterGender("Мужской");
        profilePage.submit();
    }

    @Test(priority = 3)
    public void birthdayChange() {

        ProfilePage profilePage = new ProfilePage(driver);

        buff = "2011-11-04";

        profilePage.enterBirthday("04-11-2011");
        profilePage.submit();

        Assert.assertEquals(profilePage.birthday.getAttribute("value"), buff);
    }

    @Test(priority = 4)
    public void startingDayChange() {

        ProfilePage profilePage = new ProfilePage(driver);

        buff = "2020-12-04";

        profilePage.enterStartingDay("04-12-2020");
        profilePage.submit();

        Assert.assertEquals(profilePage.birthday.getAttribute("value"), buff);
    }

    @Test(priority = 5)
    public void hobbiesChange() {

        ProfilePage profilePage = new ProfilePage(driver);

        buff = "Cookies and working";

        profilePage.enterHobbies(buff);
        profilePage.submit();

        Assert.assertEquals(profilePage.hobbies.getText(), buff);
    }

    @Test(priority = 6)
    public void innChange() {

        ProfilePage profilePage = new ProfilePage(driver);

        buff = "123456789123";

        profilePage.enterInn(buff);
        profilePage.submit();

        Assert.assertEquals(profilePage.inn.getAttribute("value"), buff);
    }

}
