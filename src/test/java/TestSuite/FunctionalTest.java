package TestSuite;


import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class FunctionalTest {

    protected static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        WebDriverManager.chromedriver().setup();

        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @AfterClass
    public static void tearDown() {
        driver.close();
    }

    @AfterMethod
    public static void takeSnapshot(ITestResult result) {
        if (result.isSuccess()) {
            return;
        }
        else {
            try {
                int n = 0;
                File theDir = new File("screenshots/test" + n);
                do {
                    if (theDir.exists()) {
                        n = n + 1;
                        theDir = new File("screenshots/test" + n);
                    }
                    else { theDir.mkdir(); }
                } while (!theDir.exists());

                File screenShot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
                int random = new Random().nextInt(200);
                FileUtils.copyFile(screenShot, new File(theDir.getAbsolutePath() + "/test" + random + ".jpeg"));
            }
            catch (SecurityException | IOException se) {
                System.out.println(se.getMessage());
            }
        }
    }

}