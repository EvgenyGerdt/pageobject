package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage extends PageObject {

    @FindBy(xpath = "/html/body/div[1]/div[2]/ul")
    private WebElement userBar;

    @FindBy(xpath = "//a[contains(text(),'Личный кабинет')]")
    private WebElement profileButton;

    public MainPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public Boolean userBar() {
        return userBar.isDisplayed();
    }
}
