package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class ProfilePage extends PageObject {

    @FindBy(xpath = "//tbody/tr[2]/td[2]/input[1]")
    public WebElement username;

    @FindBy(xpath = "//tbody/tr[3]/td[2]/select[1]")
    public Select gender;

    @FindBy(xpath = "//tbody/tr[4]/td[2]/input[1]")
    public WebElement birthday;

    @FindBy(xpath = "//tbody/tr[5]/td[2]/input[1]")
    public WebElement startingDay;

    @FindBy(xpath = "//tbody/tr[6]/td[2]/textarea[1]")
    public WebElement hobbies;

    @FindBy(xpath = "//tbody/tr[7]/td[2]/input[1]")
    public WebElement inn;

    @FindBy(xpath = "//tbody/tr[8]/td[2]/input[1]")
    private WebElement submitButton;

    public ProfilePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void enterNewUsername(String username) {
        this.username.clear();
        this.username.sendKeys(username);
    }

    public void enterGender(String gender) {
        this.gender.deselectByVisibleText(gender);
    }

    public void enterBirthday(String birthday) {
        this.birthday.clear();
        this.birthday.sendKeys(birthday);
    }

    public void enterStartingDay(String startingDay) {
        this.startingDay.clear();
        this.startingDay.sendKeys(startingDay);
    }

    public void enterHobbies(String hobbies) {
        this.hobbies.clear();
        this.hobbies.sendKeys(hobbies);
    }

    public void enterInn(String inn) {
        this.inn.clear();
        this.inn.sendKeys(inn);
    }

    public MainPage submit() {
        submitButton.click();
        return new MainPage(driver);
    }

}
