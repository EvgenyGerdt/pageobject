package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends PageObject{

    @FindBy(xpath = "/html/body/div[3]/div[1]/div[1]/form/table/tbody/tr[1]/td[2]/input")
    private WebElement email;

    @FindBy(xpath = "/html/body/div[3]/div[1]/div[1]/form/table/tbody/tr[2]/td[2]/input")
    private WebElement password;

    @FindBy(xpath = "/html/body/div[3]/div[1]/div[1]/form/table/tbody/tr[3]/td[2]/input")
    private WebElement submitButton;

    public LoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void enterEmail(String email) {
        this.email.clear();
        this.email.sendKeys(email);
    }

    public void enterPassword(String password) {
        this.password.clear();
        this.password.sendKeys(password);
    }

    public MainPage submit() {
        submitButton.click();
        return new MainPage(driver);
    }

}
